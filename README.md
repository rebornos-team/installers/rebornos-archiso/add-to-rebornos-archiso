# Add to RebornOS archiso

Files to add to RebornOS archiso

Key removed from **101_gnome.gschema.override** (No longer exists):

```
[org.gnome.settings-daemon.plugins.xsettings]
antialiasing='rgba'
```

```
# ========== Added by Rafael (RebornOS) ========== #
#
# Delete lightdm config:
# rm  airootfs/etc/lightdm/lightdm-gtk-greeter.conf
#
# Change archiso_head.cfg:
# rm syslinux/archiso_head.cfg
# syslinux/archiso_head.cfg
#
# Change archiso_sys.cfg:
# rm syslinux/archiso_sys.cfg
# syslinux/archiso_sys.cfg
#
# Add Arch Linux mirrorlist:
# airootfs/etc/pacman.d/mirrorlist
#
# Add RebornOS mirrorlist:
# airootfs/etc/pacman.d/reborn-mirrorlist
#
# Add gnupgp's (Arch and RebornOS):
# airootfs/etc/pacman.d/gnupg/pubring.gpg
# airootfs/etc/pacman.d/gnupg/trustdb.gpg
#
# GNOME Gschema:
# mkdir -p airootfs/usr/share/glib-2.0/schemas
# airootfs/usr/share/glib-2.0/schemas/101_gnome.gschema.override
#
# Add group file:
# airootfs/etc/group
#
# Add gshadow file:
# airootfs/etc/gshadow
#
# Add passwd (replace existing file):
# rm /etc/passwd
# airootfs/etc/passwd
#
# Add shadow file (replace existing file):
# rm /etc/shadow
# airootfs/etc/shadow
#
# Enable autologin for gdm (rebornos user, no password for the user):
# mkdir -p airootfs/etc/gdm
# airootfs/etc/gdm/custom.conf
#
# Add rebornos file to /var/lib/AccountsService/users/ for automatic login in gnome-xorg
# mkdir -p airootfs//var/lib/AccountsService/users
# airootfs/var/lib/AccountsService/users/rebornos
#
# Delete motd file and add empty one
# rm airootfs/etc/motd
# echo >> airootfs/etc/motd
#
# File permissions:
# chmod 644 all files
#
# ========== End Added by Rafael (RebornOS) ========== #
```

